#include<iostream>

template<typename Type>
class Stack
{
private:
	Type* StackPtr;
	int Size;
	Type Top;
public:
	Stack(int n);
	~Stack();
	bool Push(const Type);
	bool Pop();
	void PrintStack();
};

template <typename Type>
Stack<Type>::Stack(int s)
{
	Size = s;
	StackPtr = new Type[Size];
	Top = 0;
}

template <typename Type>
bool Stack<Type>::Push(const Type value)
{
	if (Top == Size)
		return false;

	StackPtr[Top++] = value;
		
	return true;
}


template <typename Type>
bool Stack<Type>::Pop()
{
	if (Top > 0)
		StackPtr[--Top];
	else
		return false;

	
	return true;
}

template <typename Type>
void Stack<Type>::PrintStack()
{
	std::cout << "Your Stack:" << '\n';
	for (int i = Top - 1; i >= 0; i--)
		std::cout << " " << StackPtr[i] << '\n';
}

template <typename Type>
Stack<Type>::~Stack()
{
	delete[] StackPtr;
}

int main()
{	
	int size;
	std::cout << "Enter Stack Size:";
	std::cin >> size;

	Stack <int> SStack(size);
	std::cout << "Enter Stack Elements:";
	int el = 0;
	while (el++ != size)
	{
		int temp;
		std::cin >> temp;
		SStack.Push(temp);
	}

	SStack.PrintStack();

	SStack.Pop();

	SStack.PrintStack();

	SStack.~Stack();


	return 0;
}